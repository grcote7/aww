<?php

namespace Aww\Partials;

?>
<nav>
	<ul>
		<li><a href="/pages/realisations">Nos réalisations</a></li>
		<li><a href="/pages/equipe">L'Équipe</a></li>
		<li><a href="/pages/contact">Contactez-nous !</a></li>
	</ul>
</nav>
<?php

namespace Aww\Partials;

?>

<footer>

	<div class="footer-content">

		<div class="bloc footer-service">
			<h3>AnyWhereWeb</h3><br>
			<ul class="footer-list">
				<li><a href="../../pages/accueil#agence">À propos</a></li>
				<li><a href="../../pages/mentions_legales/">Mentions légales</a></li>
				<li><a href="../../pages/mentions_legales#rgpd">RGPD</a></li>
				<li><a href="../../pages/contact/">Nous contacter</a></li>
			</ul>
		</div>

		<div class="bloc footer-contact">
			<h3>Nos coordonnées</h3><br>
			<ul class="footer-list">

				<li>Téléphone: 03 03 03 03 03</li>
				<li>E-mail: AnyWhereWeb21@gmail.com</li>
				<li>Adresse:<br>
					8C, Rue Jeanne Barret<br>
					21000 - Dijon <br>
					<a href="../../pages/map">(Nous trouver)</a>
				</li>
			</ul>
		</div>

		<div class="bloc footer-reseaux">
			<h3>Suivez nous !</h3><br>
			<ul class="footer-list" id="reseaux">
				<li><a href="https://twitter.com/AnyWhereWeb"><img src="../../assets/image/twiter.png"
							alt="logo twitter">Twitter</a></li>
				<li><a href="https://www.instagram.com/anywhereweb/"><img src="../../assets/image/insta.png"
							alt="logo instagram">Instagram</a></li>
				<li><a href=" https://www.linkedin.com/in/any-whereweb-b6978a243/"><img src="../../assets/image/ln.png"
							alt="logo linkedin">
						<div>Linkedin</div>
					</a></li>
			</ul>

		</div>
	</div>

</footer>

</body>

</html>
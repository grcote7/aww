<?php

/**
 * (ɔ) Online FORMAPRO - AWW - Solene, Greg & Lionel - 2022.
 */

namespace Aww\Classes;

/**
 * Clean a string of any HTML tag.
 */
class Nett
{
	public static function ss($var)
	{
		foreach ($var as $v) {
			if ($v != strip_tags($v)) {
				return 0;
			}
		}

		return 1;
	}
}
